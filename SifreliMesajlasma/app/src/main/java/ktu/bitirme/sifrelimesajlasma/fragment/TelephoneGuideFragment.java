package ktu.bitirme.sifrelimesajlasma.fragment;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import ktu.bitirme.sifrelimesajlasma.R;
import ktu.bitirme.sifrelimesajlasma.activity.ConversationActivity;

/**
 * Created by mcanv on 10.03.2017.
 */

public class TelephoneGuideFragment extends android.support.v4.app.Fragment {

   // SearchView searchView;
    ArrayList<String> dataName = new ArrayList<String>();
  //  List<String> persons;
 //   List<String> phones;
    String phoneNo;
    ListView contactList;


    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = layoutInflater.inflate(R.layout.fragment_telephoneguide, container, false);


        contactList = (ListView) rootView.findViewById(R.id.telephoneList);

        contactList.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> arg0,View arg1,int position,long arg3){
                try{
                    String phoneName = dataName.get(position).substring(0, dataName.get(position).indexOf("\n",1));
                    phoneNo = dataName.get(position).substring(dataName.get(position).indexOf("\n", 1) + 1);
                 //   phoneNo = phoneNo.substring(0, phoneNo.indexOf("-", 1)).replaceAll("\\s", "");

                    Bundle phoneData = new Bundle();
                    phoneData.putString("phoneNumber",phoneNo);
                    phoneData.putString("personName",phoneName);
                    Intent intentObject = new Intent();
                    intentObject.setClass(getActivity().getApplication(),ConversationActivity.class);
                    intentObject.putExtras(phoneData);
                    getActivity().startActivity(intentObject);
                }catch (Exception e) {
                    Toast.makeText(getActivity().getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
                    System.out.print("Burada hata var");
                }
            }
        });
        contactList.setAdapter(new ArrayAdapter<String>(getActivity().getApplicationContext(),R.layout.contactlist,R.id.contactName,dataName));
        contactList.setFastScrollEnabled(true);

        return rootView;
    }
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        loadContacts("");


    }
    public void loadContacts(String match){
      //  persons = new ArrayList<String>();
      //  phones = new ArrayList<String>();

        String selection = ContactsContract.Contacts.IN_VISIBLE_GROUP + " ="+("1")+ "";
        String sortOrder = ContactsContract.Contacts.DISPLAY_NAME + " COLLATE LOCALIZED ASC";
        Cursor contacts = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,selection,null,sortOrder);

        dataName.clear();
        HashSet<String> hs = new HashSet<String>();

        while (contacts.moveToNext()){
            String name = contacts.getString(contacts.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String number = contacts.getString(contacts.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));


            if(name!=null || number!=null){
                if(match.trim().length()==0){
                   // persons.add(name+ "\n\n" + number);
                   // persons.add(name);
                    dataName.add(name + "\n" + number);
                }
                else if (match.trim().length()!=0)
                    if(name.toLowerCase().contains(match.toLowerCase())){
                      //  phones.add(name+ "\n\n" + number);
                     //   phones.add(name);
                         dataName.add(name + "\n" + number);
                    }
            }
        }
        hs.addAll(dataName);
        Set tSet = new TreeSet(hs);
        dataName.clear();
        dataName.addAll(tSet);
        contacts.close();


    }
}
