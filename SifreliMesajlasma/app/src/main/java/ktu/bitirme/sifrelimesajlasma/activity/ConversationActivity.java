package ktu.bitirme.sifrelimesajlasma.activity;


import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import ktu.bitirme.sifrelimesajlasma.Algorithm.Crypt;
import ktu.bitirme.sifrelimesajlasma.R;
import ktu.bitirme.sifrelimesajlasma.other.DiscussArrayAdapter;
import ktu.bitirme.sifrelimesajlasma.other.OneComment;

/**
 * Created by mcanv on 15.03.2017.
 */

public class ConversationActivity extends Activity {
    static ListView conversationList;
    static DiscussArrayAdapter adapter;
    static String phoneNumber, personName;
    EditText message;
    ImageButton sentMessage;

    public Context mcontext;
    static Crypt cryptObject = new Crypt();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);

        mcontext = this;
        sentMessage = (ImageButton) findViewById(R.id.sentMessage);
        message = (EditText) findViewById(R.id.message);
        conversationList = (ListView) findViewById(R.id.conversationList);

        Bundle phoneData = getIntent().getExtras();
        phoneNumber = phoneData.getString("phoneNumber");
        personName = phoneData.getString("personName");
        this.setTitle(personName);

        /*Telefon numarısı önüne +9 eki varsa kaldılır*/
        if (phoneNumber.substring(0, 2).equals("+9"))
            phoneNumber = phoneNumber.substring(2);

        adapter = new DiscussArrayAdapter(getApplicationContext(), R.layout.discuis_listitem);

        /*Gelen kutusuna erişim*/
        Uri inboxUri = Uri.parse("content://sms/");

        /*Alınacak olan bilgiler: mesajı gönderen,mesaj içeriği,tarih ve mesajın gelen bir mesaj mı yoksa giden bir mesaj mı
        * olduğunu belirten tip bilgisi*/
        String[] columns = new String[]{"_id","address", "body", "date", "type"};
        /*Şifreli mesajlar veritabanından çekilir ve deşifreleme işleminin ardından ekranda listelenir*/
        ContentResolver cr = getContentResolver();
        Cursor c = cr.query(inboxUri, columns, "address like '%" + phoneNumber + "'", null, "date ASC");

        while (c.moveToNext()) {
            String body = c.getString(c.getColumnIndexOrThrow("body"));
            String date = c.getString(c.getColumnIndexOrThrow("date"));
            String type = c.getString(c.getColumnIndexOrThrow("type"));

            if (body.trim().length() == 0) {
                try {
                    String decryptedText = cryptObject.decrpyt(body);
                    if (decryptedText.trim().length() != 0) {
                        SimpleDateFormat formatter = new SimpleDateFormat("dd,MMM HH:mm");
                        date = formatter.format(new Date(Long.parseLong(date)));

                        /*Mesaj giden bir mesaj ise farklı bir tasarımda gelen bir mesaj ise
                        * farklı bir tasarımda görüntülenmesini sağlar.Bu tasarım baloncuk
                        * tasarımıdır.*/
                        if (type.contains("1"))
                            adapter.add(new OneComment(true, decryptedText + "\n" + date));
                        else
                            adapter.add(new OneComment(false, decryptedText + "\n" + date));
                    }
                } catch (Exception e) {

                }
            }
        }
        conversationList.setAdapter(adapter);
        conversationList.setSelection(adapter.getCount() - 1);

        sentMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    SmsManager smsManager = SmsManager.getDefault();
                    /*Mesaj gönderilmeden önce şifrelemi işlemini gerçekleştirilir*/
                    String encryptedText = cryptObject.encrypt(message.getText().toString());

                    /*Mesaj gönderilir*/
                    smsManager.sendTextMessage(phoneNumber, null, encryptedText, null, null);

                    SimpleDateFormat formatter = new SimpleDateFormat("dd,MMM HH:mm");
                    String date = formatter.format(Calendar.getInstance().getTime());

                    /*Gönderilen mesaj ekrandaki listeye eklenilir.*/
                    adapter.add(new OneComment(false, message.getText().toString().trim()
                            + "\n" + date));
                    conversationList.setAdapter(adapter);
                    conversationList.setSelection(adapter.getCount() - 1);

                    message.setText("");
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "SMS gonderilemedeki,Tekrar Deneyin!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        });

    }

    /*Yeni mesaj geldiğinde tetiklenir.Gelen mesaj önce deşifre edilir.ardından ekran listelenir.*/
    public static void updateAdapter(String message, String type, String phoneNo) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd,MMM HH:mm");
        String date = formatter.format(Calendar.getInstance().getTime());

        String decryptedText = cryptObject.decrpyt(message);
        if (phoneNo.contains(phoneNumber)) {
            if (type.contains("1"))
                adapter.add(new OneComment(true, decryptedText + "\n" + date));
            else
                adapter.add(new OneComment(false, decryptedText + "\n" + date));
            conversationList.setAdapter(adapter);
            conversationList.setSelection(adapter.getCount() - 1);
        }
    }

}
