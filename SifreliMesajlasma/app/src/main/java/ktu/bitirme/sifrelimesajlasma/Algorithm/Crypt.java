package ktu.bitirme.sifrelimesajlasma.Algorithm;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

/**
 * Created by mcanv on 15.03.2017.
 */

public class Crypt {
    Permutation permutationObject = new Permutation();
    Ceaser caesarObject = new Ceaser();

    /*
	 * şifreleme metodunda, gönderilecek bir mesaj metnine öncelikle permutasyon
	 * şifreleme sonra ise sezar şifreleme işlemi uygulanır. şifre çözme
	 * metodunda ise tam ters olarak önce sezar şifre çözme ardından permutasyon
	 * şifre çözme işlemi uygulanır.
	 */
	/*
	 * şifreleme metodunda öncelikle şifrelenecek mesajdaki boşluk karakterleri
	 * ile £ karakteri yer değiştirir. Ardindan 1-9 arasi rastgele iki sayi
	 * şifrelenecek metnin başına ve sonuna yerleştirilir. şifrelenecek metnin
	 * uzunluğu, şifreleme işleminde kullanilacak olan anahtarın uzunlugunun
	 * katı olması gerektiğinden Mod alınır ve eklenmesi gereken sayı orannnda
	 * şifrelenecek metne ' karakteri eklenir. Ardindan metin değeri permutasyon
	 * şifreleme metoduna sokulur. Metottan çıkan şifreli metin, sezar şifreleme
	 * metoduna 12 karakter kaydirma işlemi için sokulur.
	 */
    public String encrypt(String data){
        String permutationKey ="743682519";
        String encryptedText ="";

        try {
            if (data.length()!=0){
                String boslukBirakilmis = data.replaceAll("\\s+","£");
                Random rand = new Random();
                int n=rand.nextInt(9)+1;
                int m=rand.nextInt(9)+1;
                boslukBirakilmis = n + boslukBirakilmis+m;
                int mod =(permutationKey.length()-(boslukBirakilmis.length()%permutationKey.length()))%permutationKey.length();
                while (mod!=0){
                    boslukBirakilmis +="'" ;
                    mod -=1;
                }
                String encryptedText1 = permutationObject.encrpytedData(boslukBirakilmis, permutationKey);
                encryptedText1 = encryptedText1.toLowerCase();
                encryptedText = caesarObject.encryption(encryptedText1, 12);
            }
        }catch (Exception e){

        }
        return encryptedText;
    }
    /*
	 * şifre çözme metodunda ise öncelikle sezar şifreleme yöntemi ile alfabe
	 * sayisi - 12 karakter kaydirma işlemi uygulanarak şifreli metin
	 * karakterleri eski yerlerine geri getirilir. Ardından elde edilen şifreli
	 * metin, permutasyon şifre çözme işlemi için çözü-ücü anahtar değeri ile
	 * yeniden şifreleme metoduna gönderilir ve açık metin elde edilir. Açık
	 * metindeki £ karakterlerinin yerlerine boşluk karakteri eklenir. Metnin
	 * sonundan, varsa eğer mod işlemi sırasında eklediğimiz ' karakterleri
	 * çıkkartılır. Açık metnin başında ve sonunda şifreleme işlemi sırasında
	 * eklediğimiz rastgele sayılar kalacaktır. Bu sayı değerleri de açık
	 * metinden çıkartılarak mesajın ilk hali elde edilir.
	 */
    public String decrpyt(String data){
        String permutationDeKey ="863274159";
        String decryptedText="";
        try {
            decryptedText = caesarObject.encryption(data,caesarObject.alphabet.length()-12);
            decryptedText = permutationObject.encrpytedData(decryptedText,permutationDeKey);
            decryptedText = decryptedText.replaceAll("£","");
            decryptedText = decryptedText.replaceAll("","");
            decryptedText.trim();
            if (tryParseInt(decryptedText.substring(0,1))
                    && tryParseInt(decryptedText.substring(decryptedText.length()-1))){
                decryptedText = decryptedText.substring(1);
                decryptedText = decryptedText.substring(0,decryptedText.length()-1);
            }else decryptedText="";
        }catch (Exception e){
            decryptedText = "";
        }
        return decryptedText;
    }
    /*Kendisine gelen değerin sayi olup olmadığını kontrol eder.*/
    private static boolean tryParseInt(String value){
        try {
            Integer.parseInt(value);
            return true;
        }catch (NumberFormatException nfe){
            return false;
        }
    }
}
