package ktu.bitirme.sifrelimesajlasma.Algorithm;

/**
 * Created by mcanv on 15.03.2017.
 */

public class Ceaser {
    /*Şifreleme işlemind ekullanılan alfabe*/
    String alphabet = "abcçcdefgğhıiklmnoöprsştuüvwxyz1234567890+*=%£!@#$$/&()-':;.?<>|{}[]^é½``§€";

    /*Metod içersine şifrelenecek metni ve kaydırma miktarını alır*/
    public String encryption(String plainText,int shift){
        String cipherText ="";
        for (int i = 0; i <plainText.length() ; i++) {

            /*Eğer şifrelenecek metin sonuna gelindiyse aşağıdaki if döngüsüne girer*/
            if (plainText.charAt(i)==' '){
                cipherText +=" ";
                continue;
            }
            /*
            * Eğer metin karakterinin index değeri < (alfabe sayısı-shift-1)
            * ise yani ilgili karakterin index değerinin üzerine kaydırma
                    * değerini (shift) eklediğimizde hala alfabede içerisinde olan bir
                    * harfin index değerini elde edebiliyorsak, yeni harf değeri, eski
                    * harfin index değeri üzerine kaydırma miktarı eklenerek elde
            * edilir.
            */
            if (alphabet.indexOf(plainText.charAt(i)) <= alphabet.length() - shift - 1)
                cipherText += alphabet.charAt(alphabet.indexOf(plainText.charAt(i)) + shift);
            /*Eğer,ilgili karakterin index değeri + kaydırma değeri > alfaba sayısı ise ilgili
            * karakterin yerine geçen yeni değeri bulamka için alfabenin başına dönmek gerekir.*/
            else
                cipherText += alphabet.charAt(alphabet.indexOf(plainText.charAt(i)) + shift - alphabet.length());
        }
        return cipherText;
    }
}
