package ktu.bitirme.sifrelimesajlasma.fragment;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Contacts;
import android.provider.Telephony;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import java.util.ArrayList;
import java.util.List;

import ktu.bitirme.sifrelimesajlasma.Algorithm.Crypt;
import ktu.bitirme.sifrelimesajlasma.R;
import ktu.bitirme.sifrelimesajlasma.activity.ConversationActivity;

/**
 * Created by mcanv on 11.03.2017.
 */

public class MessageListFragment extends android.support.v4.app.Fragment {
    SimpleCursorAdapter adapter;
    ListView messageList;
    List<String> persons;
    List<String> phones;
    ArrayList<String> dataName;
    Crypt cryptObject = new Crypt();
    ContentResolver cr;
    Cursor c;
    Uri inboxURI;
    String[] columns;

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView =  layoutInflater.inflate(R.layout.fragment_messagelist, container, false);
        messageList = (ListView) rootView.findViewById(R.id.listView1);
        /*Mesaj iletisi boş ise bu duruma dair bir yazı görüntülenir*/
        messageList.setEmptyView(messageList.findViewById(R.id.empty));

        messageList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                try {
                    Bundle phoneData = new Bundle();
                    phoneData.putString("phoneNumber",phones.get(position)+"");
                    phoneData.putString("personName",persons.get(position)+"");
                    Intent intentObject = new Intent();
                    intentObject.setClass(getActivity().getApplication(), ConversationActivity.class);
                    intentObject.putExtras(phoneData);
                    startActivity(intentObject);
                }catch (Exception e){

                }
            }
        });
        messageList.setAdapter(new ArrayAdapter<String>(getActivity().getApplicationContext(), R.layout.list_message_icon_name,R.id.contactName2,persons));
        messageList.setFastScrollEnabled(true);
        return rootView;

    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        /*Gelen kutusuna erişim Uri'si*/
        inboxURI = Uri.parse("content://sms");

        /*Alınacak olan bilgiler id,mesajı gönderen,mesaj içeriği ve okunup okunmadığı bilgisi*/
        columns = new String[] {"_id","address","body","read"};

        cr = getActivity().getContentResolver();

        /*Bu metot sayesinde daha öncden mesajlaşmış kişiler listelenir.*/
        adapterLoad();


    }
    /*Bu metot veritabanında mesajları çekerek şifreli mesajların ekranda görüntülenmesini sağlar*/
    public void adapterLoad(){
        persons = new ArrayList<String>();
        phones = new ArrayList<String>();
        dataName = new ArrayList<String>();
     //   messageList.setAdapter(null);

        c = cr.query(inboxURI,columns,null,null,null);
        dataName.clear();
        //Şifreleme algoritması burada kullanılacak
        while (c.moveToNext()){
            try {
                String body = c.getString(c.getColumnIndexOrThrow("body"));
                String message = cryptObject.decrpyt(body);

                if (message.length()!=0){
                    String read = c.getString(c.getColumnIndexOrThrow("read"));
                    String senderPhone = c.getString(c.getColumnIndexOrThrow("address"));
                    String senderName = getContactName(senderPhone);

                    if (senderName.trim().length()==0 && !persons.contains(senderPhone)){
                        persons.add(senderPhone);
                        phones.add(senderPhone);
                        dataName.add(senderPhone + "-" + senderPhone + "-" + read);
                    }
                    if (senderName.trim().length()==0 && !persons.contains(senderName)){
                        persons.add(senderName);
                        phones.add(senderPhone);
                        dataName.add(senderPhone + "-" + senderPhone + "-" + read);
                    }
                }
            }catch (Exception e) {}
        }

    }
    /*Bu metot içersine gelen telefon numarası bilgisine sahip olan kişinin adını döndürür.*/
    public String getContactName(final String phoneNumber){
        Uri uri;
        String[] projection;
        Uri mBaseUri = Contacts.Phones.CONTENT_FILTER_URL;
        projection = new String[] {Contacts.People.NAME};
        try{
            Class<?> c = Class.forName("android.provider.ContactsContract$PhoneLookup");
            mBaseUri = (Uri) c.getField("CONTENT_FILTER_URL").get(mBaseUri);
            projection = new String[] {"display_name"};
        }catch (Exception e) {}
        uri = Uri.withAppendedPath(mBaseUri,Uri.encode(phoneNumber));
        Cursor cursor = this.getActivity().getContentResolver().query(uri,projection,null,null,null);

        String contactName = "";

        if (cursor.moveToNext()){
            contactName = cursor.getString(0);
        }
        cursor.close();


        return contactName;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        /*Üst kisimda bulunan yenile,telefon rehber ve güncelleme
        * butonularının işlevlerinin belirtildiği metot*/
        switch (item.getItemId()){
            case R.id.reflesh:
                adapterLoad();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    //@Override
   // public void onCreateOptionsMenu(Menu menu,MenuInflater menuInflater){
        /*Üst kısımdaki butunların tasarımını içeren menu*/
       // MenuInflater menuInflater2 = getActivity().getMenuInflater();
    //    menuInflater.inflate(R.menu.messagelist_menu,menu);
   //     super.onCreateOptionsMenu(menu,menuInflater);

    //}

}
