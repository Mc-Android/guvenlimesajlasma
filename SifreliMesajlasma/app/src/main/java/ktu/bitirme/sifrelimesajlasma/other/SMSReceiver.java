package ktu.bitirme.sifrelimesajlasma.other;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;

import ktu.bitirme.sifrelimesajlasma.activity.ConversationActivity;

/**
 * Created by mcanv on 15.03.2017.
 */

public class SMSReceiver extends BroadcastReceiver {
    /*SmsManager nesnesi oluşturulur*/
    final SmsManager sms = SmsManager.getDefault();

    public void onReceive(Context context, Intent intent){

        /*Cihaza bir mesaj geldiğinde bu metot tetiklenir*/
        Bundle bundle = intent.getExtras();
        SmsMessage[] smsm = null;
        String sms_str ="";

        if (bundle!=null){
            /*SMS mesaj elde edilir*/
            Object[] pdus = (Object[]) bundle.get("pdus");
            smsm = new SmsMessage[pdus.length];
            for (int i = 0; i <smsm.length ; i++) {
                smsm[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                sms_str += smsm[i].getMessageBody().toString();
            }
            try {
                /*Conversation sınıfındaki updateAdapter metoduna mesaj,"1" değeri(mesajın
                * ekranda doğru tasarımla görünmesi için) ve mesajı gönderen kişinin telefon
                * numarası bilgilsini gönderilir*/
                ConversationActivity.updateAdapter(sms_str,"1",smsm[0].getOriginatingAddress());
            }catch (Exception e){

            }
        }
    }

}
