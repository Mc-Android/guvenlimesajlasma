package ktu.bitirme.sifrelimesajlasma.Algorithm;

/**
 * Created by mcanv on 15.03.2017.
 */

public class Permutation {

    /*Şifrelenecek mesaj bloklara ayrılır ve şifrleme metoduna gönderilir.*/
    public String encrpytedData(String data,String key){
        String encryptedText ="";
        while (data.length()!=0){
            String block = null;
            block = data.substring(0,key.length());
            data=data.substring(key.length());
            encryptedText+=encrypt(block,key);
        }
        return encryptedText;
    }
    /*Anahtar değerinin sıralamasına göre şifreli yeni block değerleri elde edilir*/
    private String encrypt(String data,String key){
        int keyValue =0;
        String cipherText="";
        for (int i = 0; i <data.length() ; i++) {
            keyValue = Integer.parseInt(String.valueOf(key.charAt(i)));
            cipherText+=data.charAt(keyValue-1);
        }
        return cipherText;
    }
}
