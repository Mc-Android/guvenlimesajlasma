package ktu.bitirme.sifrelimesajlasma.activity;

import android.app.Activity;
import android.os.Bundle;

import ktu.bitirme.sifrelimesajlasma.R;

/**
 * Created by mcanv on 11.03.2017.
 */

public class AboutUsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
    }
}
