package ktu.bitirme.sifrelimesajlasma.other;

/**
 * Created by mcanv on 15.03.2017.
 */

public class OneComment {
    public boolean left;
    public String comment;

    public OneComment(boolean left, String comment) {
        super();
        this.left = left;
        this.comment = comment;
    }
}
