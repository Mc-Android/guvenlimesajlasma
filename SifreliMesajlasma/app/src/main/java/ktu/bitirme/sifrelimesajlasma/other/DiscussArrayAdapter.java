package ktu.bitirme.sifrelimesajlasma.other;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ktu.bitirme.sifrelimesajlasma.R;

/**
 * Created by mcanv on 15.03.2017.
 */

public class DiscussArrayAdapter extends ArrayAdapter<OneComment> {

    private TextView message;
    private List<OneComment> oneCommentList = new ArrayList<OneComment>();
    private LinearLayout wrapper;

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer){
        if (observer != null){
            super.unregisterDataSetObserver(observer);
        }
    }
    @Override
    public void add(OneComment objects){
        oneCommentList.add(objects);
        super.add(objects);
    }
    public DiscussArrayAdapter(Context context, int textViewResoruceId){
        super(context,textViewResoruceId);
    }
    public int getCount(){
        return this.oneCommentList.size();
    }
    public OneComment getItem(int index){
        return this.oneCommentList.get(index);
    }
    public View getView(int position, View converView, ViewGroup parent){
        View row = converView;
        if (row==null){
            LayoutInflater inflater= (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row =inflater.inflate(R.layout.discuis_listitem,parent,false);
        }
        wrapper = (LinearLayout) row.findViewById(R.id.wrapper);
        OneComment coment = getItem(position);
        message = (TextView) row.findViewById(R.id.comment);
        message.setText(coment.comment);
        message.setBackgroundColor(coment.left ? Color.GREEN : Color.YELLOW);
        wrapper.setGravity(coment.left ? Gravity.LEFT : Gravity.RIGHT);

        return row;
    }
    public Bitmap decodeToBitmap(byte[] decodeByte){
        return BitmapFactory.decodeByteArray(decodeByte,0,decodeByte.length);
    }
}
